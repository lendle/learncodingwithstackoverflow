const csvtojson = require('csvtojson');
const fs = require('fs');
const natural = require('natural');
const tokenizer = new natural.WordTokenizer();
const mathjs = require('mathjs');
const { Parser } = require('json2csv');
const similarity = require('compute-cosine-similarity');

async function collectTagForClusters() {
    let _json = fs.readFileSync("javascript_questions.json");
    let items = JSON.parse(_json);

    let tagsInfo = JSON.parse(fs.readFileSync("filteredTokens.json"));
    let tags = {};
    for (let tag of tagsInfo) {
        tags[tag.tag] = tag.count;
    }

    let results = [];
    let json = await csvtojson().fromFile("javascript_question_cluster.csv");
    //add the title property back
    for (let i = 0; i < json.length; i++) {
        json[i]["title"] = items[json[i].index].title;
    }
    //iterate through clusters, figure out general tags for each group
    for (let cluster = 1; cluster <= 100; cluster++) {
        console.log("processing cluster: " + cluster);
        let tfidf = new natural.TfIdf();
        let array = [];//array keeps questions in this cluster
        for (let i = 0; i < json.length; i++) {
            if (parseInt(json[i].cluster) == cluster) {
                array.push(json[i]);
            }
        }
        console.log(array.length);

        let allTokens = {};
        let documents = {};
        for (let i of array) {
            let title = i.title;
            let questionIndex = i.index;
            let tokens = items[questionIndex].tags;
            for (let token of tokens) {
                allTokens[token] = 0;
            }
            tfidf.addDocument(title);
            documents[questionIndex] = {
                index: i.index,
                title: title,
                tags: []
            };
        }
        //re-calculate tfidf score (but against only documents in this cluster)
        for (let token in allTokens) {
            if (token.length > 2 && token.match(/[^$\d]/)) {
                tfidf.tfidfs(token, function (i, measure) {
                    if (measure > 0) {
                        documents[array[i].index].tags.push({
                            name: token,
                            measure: measure
                        });
                    }
                });
            }
        }
        for (let i in documents) {
            documents[i].tags.sort(function (o1, o2) {
                return o1.measure - o2.measure;
            });
        }

        let generalTags = {};
        for (let i in documents) {
            let array = [];
            for (let tag of documents[i].tags) {
                if (tag.name.toLowerCase() != "javascript") {
                    array.push(tag.measure);
                }
            }
            if (array.length == 0) {
                //skip document with no usable tags
                continue;
            }
            let mean = mathjs.mean(array);
            let sd = mathjs.std(array);
            let count = 0;
            for (let tag of documents[i].tags) {
                if (tag.name.toLowerCase() == "javascript") {
                    continue;
                }
                //keep tags with lower score (thus more general)
                if (tag.measure <= mean) {
                    //generalTags[tag.name.toLowerCase()] = i;
                    //generalTags[natural.PorterStemmer.stem(tag.name).toLowerCase()] = i;
                    let stemmed=natural.PorterStemmer.stem(tag.name).toLowerCase();
                    if(!tags[stemmed]){
                        continue;
                    }
                    generalTags[stemmed] = tag.measure;
                }
                count++;
                /*if (count >= 10) {
                    //limit to 3 tags for each document
                    break;
                }*/
            }
        }
        let orderedGeneralTags = [];
        for (let i in generalTags) {
            orderedGeneralTags.push({
                tag: i,
                measure: generalTags[i]
            });
        }
        orderedGeneralTags.sort(function (o1, o2) {
            return o1.measure - o2.measure;
        });
        let moreCommonGeneralTags = {};
        for (let i = 0; /*i < 5 &&*/ i < orderedGeneralTags.length; i++) {
            moreCommonGeneralTags[orderedGeneralTags[i].tag] = orderedGeneralTags[i].measure;
        }
        //console.log(generalTags);
        results.push({
            cluster: cluster,
            tags: moreCommonGeneralTags
        });
        //console.log(json);
    }
    fs.writeFileSync("generalTags_javascript.json", JSON.stringify(results));
}
//loop detection and removal
function loopRemovel(parentChildMap){
    let loopCheckNeeded = true;
    loopDetectionAndRemoval: while (loopCheckNeeded) {
        loopCheckNeeded = false;
        for (let key in parentChildMap) {
            let path = [];
            let node = key;
            path.push(node);
            node = parentChildMap[node][0].name;//use the first candidate in the candidate parent list
            while (node != null) {
                if (key != node && path.includes(node)) {
                    path.push(node);
                    //there is a loop, try to break
                    let problemNode = path[path.length - 2];
                    let candidateList = parentChildMap[problemNode];
                    candidateList.splice(0, 1);
                    loopCheckNeeded = true;
                    continue loopDetectionAndRemoval;
                } else {
                    path.push(node);
                    if (node == "javascript") {
                        break;
                    }
                    node = parentChildMap[node][0].name;
                }
            }
        }
    }
}

function constructChildParentMap(allTags, cooccurDataAsMap, xOverYThreshold, yOverXThreshold){
    let parentChildMap = {};//child->parent tag
    for (let tag of allTags) {
        parentChildMap[tag] = [{ name: "javascript", value: 0 }];//the default root
    }
    for (let y of allTags) {
        for (let x of allTags) {
            if (x != y) {
                if (cooccurDataAsMap[y][x] >= xOverYThreshold) {
                    if (cooccurDataAsMap[x][y] <= yOverXThreshold) {
                        parentChildMap[y].push({ name: x, value: cooccurDataAsMap[y][x] });
                        break;
                    }
                }
            }
        }
    }
    //console.log(dataAsMap);
    //console.log(parentChildMap);

    //sort the candidate parent list
    for (let key in parentChildMap) {
        let candidateParent = parentChildMap[key];
        candidateParent.sort(function (o1, o2) {
            return (o1.value - o2.value) * (-1);
        });
    }
    //loop detection and removal  
    loopRemovel(parentChildMap);
    //////////////////////////////////////////////
    //re-organize the map
    let keys = [];
    for (let key in parentChildMap) {
        keys.push(key);
    }
    let similarTagGroup={};//tag=>[array of similar tags]
    for (let i = 0; i < keys.length; i++) {
        for (let j = i + 1; j < keys.length; j++) {
            let key1 = keys[i];
            let key2 = keys[j];
            if (cooccurDataAsMap[key1][key2] < 0.5/* && parentChildMap[key1][0].name == parentChildMap[key2][0].name*/) {
                let stringDistance=natural.LevenshteinDistance(key1, key2)/mathjs.max(key1.length, key2.length);
                if(stringDistance>=0.2){
                    continue;
                }
                let array1 = [];
                let array2 = [];
                for (let key in cooccurDataAsMap[key1]) {
                    if (!isNaN(cooccurDataAsMap[key1][key])) {
                        array1.push(cooccurDataAsMap[key1][key]);
                        array2.push(cooccurDataAsMap[key2][key]);
                    }
                }
                let cos = similarity(array1, array2);
                if (cos >= 0.5) {
                    //then consider the two tag as similar
                    //console.log(key1 + " vs. " + key2 + "=" + stringDistance + ":" + cooccurDataAsMap[key1][key2] + ":" + parentChildMap[key1][0].name + ":" + parentChildMap[key2][0].name + ":" + cos);
                    let group1=similarTagGroup[key1];
                    let group2=similarTagGroup[key2];
                    if(!group1){
                        group1=[];
                    }
                    if(!group2){
                        group2=[];
                    }
                    let newGroup=[...group1, ...group2];
                    newGroup.push(key1);
                    newGroup.push(key2);
                    for(let _key of newGroup){
                        similarTagGroup[_key]=newGroup;
                    }
                }
            }
        }
    }
    for (let key in parentChildMap) {
        let candidateParent = parentChildMap[key];
        parentChildMap[key] = candidateParent[0].name;
    }
    //then merge similar tags
    mergeSimilarTags(parentChildMap, similarTagGroup);
    //convert stemmed name back to original name
    let filteredTokens=JSON.parse(fs.readFileSync("filteredTokens.json"));
    let tokenNameMap={};
    for(let token of filteredTokens){
        tokenNameMap[token.tag]=token.original;
    }
    for(let key in parentChildMap){
        let parentNode=parentChildMap[key];
        delete parentChildMap[key];
        parentChildMap[tokenNameMap[key]]=tokenNameMap[parentNode];
        console.log(key+":"+tokenNameMap[key]);
    }
    manualModification(parentChildMap);
    //console.log(JSON.stringify(parentChildMap));
    return parentChildMap;
}
/*
 * apply manual modifications
 * use this to incorporate domain knowledge
 */
function manualModification(parentChildMap){
    //manual merge
    let similarTagGroup=JSON.parse(fs.readFileSync("manual_similarTagGroup.json"));
    mergeSimilarTags(parentChildMap, similarTagGroup);
    //manual parent-child relationship
    let parentChildMap2=JSON.parse(fs.readFileSync("manual_parentChildMap.json"));
    for(let key in parentChildMap2){
        parentChildMap[key]=parentChildMap2[key];
    }
}

function mergeSimilarTags(parentChildMap, similarTagGroup){
    for (let key in similarTagGroup) {
        let group=similarTagGroup[key];
        group.sort(function(o1, o2){
            return o1.length-o2.length;
        });
        let newKey=group[0];//replace every tag in group with newKey
        for(let toReplace of group){
            if(toReplace!=newKey){
                for(let _key in parentChildMap){
                    if(parentChildMap[_key]==toReplace){
                        parentChildMap[_key]=newKey;
                    }
                }
                delete parentChildMap[toReplace];
            }
        }
    }
}

function inferOntology(xOverYThreshold = 0.6, yOverXThreshold = 0.9, outputCSV = false) {
    let clusters = JSON.parse(fs.readFileSync("generalTags_javascript.json"));
    let allTags = [];
    for (let cluster of clusters) {
        let tags = cluster.tags;
        for (let tag in tags) {
            if (allTags.includes(tag) == false) {
                allTags.push(tag);
            }
        }
    }
    let data = [];
    let cooccurDataAsMap = {};//store the co-occurrence of tag pairs with regards to clusters
    for (let tag of allTags) {
        let row = {};
        data.push(row);
        row[" "] = tag;
        cooccurDataAsMap[tag] = row;

        for (let tag of allTags) {
            row[tag] = 0;
        }

        let exists = [];//clusters contain this tag
        let queryingTag = tag;
        for (let cluster of clusters) {
            let tags = cluster.tags;
            for (let tag in tags) {
                if (tag == queryingTag) {
                    exists.push(cluster);
                }
            }
        }
        let candidateMap = {};
        for (let cluster of exists) {
            for (let tag in cluster.tags) {
                if (!candidateMap[tag]) {
                    candidateMap[tag] = 1;
                } else {
                    candidateMap[tag] = candidateMap[tag] + 1;
                }
            }
        }
        for (let _tag in candidateMap) {
            let value = (candidateMap[_tag] / exists.length);
            row[_tag] = value;

        }
        if (outputCSV) {
            let fields = [" ", ...allTags];
            const opts = { fields };
            const parser = new Parser(opts);
            const csv = parser.parse(data);
            //console.log(csv);
            fs.writeFileSync("knowledgeMap_javascript.csv", csv);
        }
    }
    
    //////////////////////////////////////////////
    //console.log(JSON.stringify(parentChildMap));
    return constructChildParentMap(allTags, cooccurDataAsMap, xOverYThreshold, yOverXThreshold);
}

function outputTree(parentChildMap) {
    let tempMapForPrintTree = {};
    let tempMapForHTMLTree = {};
    for (let i in parentChildMap) {
        let child = i;
        let parent = parentChildMap[child];

        if (!tempMapForPrintTree[parent]) {
            tempMapForPrintTree[parent] = {
                name: parent,
                children: []
            };
            tempMapForHTMLTree[parent] = {
                text: { name: parent },
                children: []
            };
        }
        if (!tempMapForPrintTree[parent].children) {
            tempMapForPrintTree[parent].children = [];
            tempMapForHTMLTree[parent].children = [];
        }

        let childNode = {
            name: child
        };
        tempMapForPrintTree[parent].children.push(childNode);
        tempMapForPrintTree[child] = childNode;

        let childNodeForHTMLTree = {
            text: { name: child }
           /*,
            collapsed: (child != "javascript")*/
        };
        tempMapForHTMLTree[parent].children.push(childNodeForHTMLTree);
        tempMapForHTMLTree[child] = childNodeForHTMLTree;
    }
    const print_tree = require('print-tree');
    //console.log(JSON.stringify(tempMap["javascript"]));
    //print_tree(tempMapForPrintTree["javascript"], node => node.name,  node => node.children,);
    //generate Treant.js tree config
    let simple_chart_config = {
        chart: {
            container: "#tree-simple",
            levelSeparation: 40,
            //rootOrientation: "WEST",
            node: {
                collapsable: true
            },
            connectors: {
                type: "step"
            }
        },

        nodeStructure: tempMapForHTMLTree["javascript"]
    };
    //console.log(JSON.stringify(simple_chart_config));
    fs.writeFileSync("knowledgemap_treant.js", "var tree=" + JSON.stringify(simple_chart_config));
}
module.exports = { collectTagForClusters, inferOntology, outputTree };