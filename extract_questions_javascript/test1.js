const request=require('request-promise');
const { sleep } = require('./Common');
const math=require('mathjs');
const fs=require('fs');
const { Parser } = require('json2csv');
let url = "https://api.stackexchange.com/2.2/questions?key=TsELULFShNfv3LtUcuTk4Q((&order=desc&sort=votes&min=10&site=stackoverflow&tagged=javascript&pagesize=100";

async function execute(){
    let viewCounts=[];
    let answerCounts=[];
    let scores=[];
    let questionMeta=[];

    let questions=JSON.parse(fs.readFileSync("../javascript_tag_from_article/javascript_questions.json"));
    let filteredQuestions=[];
    for(let question of questions){
        if(question.is_answered && question.title.toLowerCase().indexOf("how")!=-1){
            filteredQuestions.push(question);
            viewCounts.push(question.view_count);
            answerCounts.push(question.answer_count);
            scores.push(question.score);
            questionMeta.push({
                view: question.view_count,
                answer: question.answer_count,
                score: question.score,
                title: question.title,
                link: question.link,
                tagNumber: question.tags.length,
                answerAccepted: !isNaN(question.accepted_answer_id)?1:0,
                creationDate: question.creation_date,
                lastEditDate: question.last_activity_date,
                duration: question.last_activity_date-question.creation_date
            });
        }
    }
    
    let meanViewCount=math.mean(viewCounts);
    let meanViewCountStd=math.std(viewCounts);
    let meanAnswerCount=math.mean(answerCounts);
    let meanAnswerCountStd=math.std(answerCounts);
    let meanScore=math.mean(scores);
    let scoreStd=math.std(scores);
    
    for(let question of filteredQuestions){
        if(question.view_count>meanViewCount+meanViewCountStd*0.5 && 
            question.answer_count>=meanAnswerCount-meanAnswerCountStd*0.5){
            console.log(question.title);
        }
    }

    console.log("meanViewCount="+meanViewCount+", std="+meanViewCountStd);
    console.log("meanAnswerCount="+meanAnswerCount+", std="+meanAnswerCountStd);
    console.log("meanScore="+meanScore+", std="+scoreStd);
    console.log(filteredQuestions.length);
    console.log(filteredQuestions[0]);

    let fields = ["view", "answer", "score", "tagNumber", "answerAccepted", "creationDate", "lastEditDate", 
        "duration", "title", "link"];
    const parser = new Parser({fields});
    const csv = parser.parse(questionMeta);
    fs.writeFileSync("questionMeta.csv", csv);
}

execute();