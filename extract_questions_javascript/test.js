const request=require('request-promise');
const { sleep } = require('./Common');
const math=require('mathjs');
let url = "https://api.stackexchange.com/2.2/questions?key=TsELULFShNfv3LtUcuTk4Q((&order=desc&sort=votes&min=10&site=stackoverflow&tagged=javascript&pagesize=100";

async function execute(){
    let questions=[];
    let viewCounts=[];
    let answerCounts=[];

    for (let page = 1; page <= 3; page++) {
        let ret = await request.get({
            uri: url + "&page=" + page,
            headers: {
                'Accept-Encoding': 'gzip'
            },
            gzip: true
        });
        let response = JSON.parse(ret);
        let items = response.items;
        for (let item of items) {
            if(item.is_answered && item.title.toLowerCase().indexOf("how")!=-1){
                //console.log(item.title+":"+item.view_count+":"+item.answer_count);
                questions.push({
                    title: item.title,
                    view_count: item.view_count,
                    answer_count: item.answer_count
                });
                viewCounts.push(item.view_count);
                answerCounts.push(item.answer_count);
            }
        }
        if (!response.has_more) {
            break;
        }

        if (page % 2 == 0) {
            console.log("wait for a while");
            await sleep(30000);
        }
    }
    let meanViewCount=math.mean(viewCounts);
    let meanAnswerCount=math.mean(answerCounts);
    console.log("meanViewCount="+meanViewCount);
    console.log("meanAnswerCount="+meanAnswerCount);
    console.log(questions);
}

execute();